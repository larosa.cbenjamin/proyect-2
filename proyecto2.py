import json
#Claudio La Rosa Navarrete, Rut: 20.834.565-6

#En esta funcion es donde ocurre lo principal del codigo, ya que de dos archivos csv se convierte solamente en un json
def conversor_archivo (archivo_uno, archivo_dos):
    data= {}
    data_dos ={}
    data['info_vacunacion'] = []
    data_dos['info_vacunacion_fabricante'] = []
    num_total_vac = 0
    num_total_esq = 0
    total_vac = 0
    contador_uno = 0
    contador_dos = 0
    c_cansino = 0
    c_johnson = 0
    c_pfizer = 0
    c_astrazeneca = 0
    c_moderna = 0
    c_sinovac = 0

    #Aqui es el proceso de crear la primera parte del archivo json
    #esto gracias a que se divide el archivo para poder entregar la informacion segun los meses y el pais
    with open(archivo_uno, "r") as archivo:
        lineas = archivo.readlines()
        for i in lineas:    
            if contador_uno != 0: 
                j = i.split(',')
                fecha = j[2].split('-')
                if j[4] == "" or j[4] == "0.0":
                    j[4] = "0"
                if j[5] == "":
                    j[5] = "0"     

                
                if contador_dos == 0:
                    mes_anterior = fecha[1]
                    pais_actual = j[0]
                    contador_dos = contador_dos + 1

                if mes_anterior != fecha[1]:
                    mes_anterior_aux = mes_anterior
                    pais_actual_aux = pais_actual

                    if pais_actual == j[0]:
                        num_total_vac = float(num_total_vac)
                        num_total_vac_aux = float(j[4])
                        num_total_vac = num_total_vac + num_total_vac_aux
                        num_total_esq = float(num_total_esq)
                        num_total_esq_aux = float(j[5])
                        num_total_esq = num_total_esq + num_total_esq_aux
                        num_abs_inm = float(num_abs_inm)
                        num_abs_inm = num_total_esq + num_total_vac
                        mes_anterior = fecha[1]

                        num_total_vac = str(num_total_vac)
                        num_total_esq = str(num_total_esq)
                        num_abs_inm = str(num_abs_inm)

                        data['info_vacunacion'].append({
                             'Pais': j[0],
                             'Anio': fecha[0],
                             'Mes': mes_anterior_aux,
                             'Numero absoluto de inmunizaciones': num_abs_inm,
                             'Numero total de vacunaciones': num_total_vac,
                             'Total de esquemas completos': num_total_esq})

                    else:
                        num_total_vac = str(num_total_vac)
                        num_total_esq = str(num_total_esq)
                        num_abs_inm = str(num_abs_inm)

                        data['info_vacunacion'].append({
                             'Pais': pais_actual_aux,
                             'Anio': fecha[0],
                             'Mes': mes_anterior_aux,
                             'Numero absoluto de inmunizaciones': num_abs_inm,
                             'Numero total de vacunaciones': num_total_vac,
                             'Total de esquemas completos': num_total_esq})

                        num_total_vac = 0
                        num_total_esq = 0
                        num_total_vac_aux = float(j[4])
                        num_total_vac = num_total_vac + num_total_vac_aux
                        num_total_esq_aux = float(j[5])
                        num_total_esq = num_total_esq + num_total_esq_aux
                        num_abs_inm = num_total_esq + num_total_vac
                        pais_actual = j[0]
                        mes_anterior = fecha[1]

                    
                    
                    
                else:
                    num_total_vac = float(num_total_vac)
                    num_total_vac_aux = float(j[4])
                    num_total_vac = num_total_vac + num_total_vac_aux
                    num_total_esq = float(num_total_esq)
                    num_total_esq_aux = float(j[5])
                    num_total_esq = num_total_esq + num_total_esq_aux
                    num_abs_inm = num_total_esq + num_total_vac
                    mes_anterior = fecha[1]

            contador_uno = contador_uno + 1

    contador_uno = 0  
    contador_dos = 0

    #Aqui es el proceso de crear la segunda parte del archivo json
    #al igual que en el primer proceso aqui se extrae la informacion necesaria 
    with open(archivo_dos, "r") as archivo:
        lineas = archivo.readlines()
        for i in lineas:
            if contador_uno != 0:
                j = i.split(',')
                fecha = j[1].split('-')
                
                if contador_dos == 0:
                    dia = fecha[2]
                    contador_dos = contador_dos + 1
                
                dia_aux = float(fecha[2])
                dia = float(dia)
                
                if dia < dia_aux:
                    pais_actual = j[0]
                    mes_anterior = fecha[1]

                    if j[2] == "Johnson&Johnson":
                        c_johnson = float(j[3])
                                                
                    elif j[2] == "Moderna":
                        c_moderna = float(j[3])
                        

                    elif j[2] == "Pfizer/BioNTech":
                        c_pfizer = float(j[3])


                    elif j[2] == "Oxford/AstraZeneca":
                        c_astrazeneca = float(j[3])
                        

                    elif j[2] == "Sinovac":
                        c_sinovac = float(j[3])
                    

                    elif j[2] == "CanSino":
                        c_cansino = float(j[3])
                    dia = fecha[2]

                dia_aux = float(fecha[2])
                dia = float(dia)
                
                if dia > dia_aux:
                    c_total = c_sinovac + c_astrazeneca
                    c_total = c_total + c_cansino + c_moderna
                    c_total = c_total + c_johnson + c_pfizer 

                    strvac = f"Moderna ({c_moderna}) Pfizer ({c_pfizer}) J&J ({c_johnson}) Cansino ({c_cansino}) Sinovac ({c_sinovac}) AstraZeneca ({c_astrazeneca})"
                    data_dos['info_vacunacion_fabricante'].append({
                                 'Pais': pais_actual,
                                 'Anio': fecha[0],
                                 'Mes': mes_anterior,
                                 'Tipos de vacunas': strvac}) 
                    dia = fecha[2]                    
                else:
                    dia = fecha[2]
                    if j[2] == "Johnson&Johnson":
                        c_johnson = float(j[3])
                                                
                    elif j[2] == "Moderna":
                        c_moderna = float(j[3])
                        

                    elif j[2] == "Pfizer/BioNTech":
                        c_pfizer = float(j[3])


                    elif j[2] == "Oxford/AstraZeneca":
                        c_astrazeneca = float(j[3])
                        

                    elif j[2] == "Sinovac":
                        c_sinovac = float(j[3])
                    

                    elif j[2] == "CanSino":
                        c_cansino = float(j[3])

            contador_uno = contador_uno + 1

    data.update(data_dos)    
    with open('country_vacc.json', 'w') as archivo:
        json.dump(data, archivo, indent = 4)    

#Esta funcion permite leer el avance completo de un pais
#un problema que no pude arreglar es que hay un error de años entre el primero y el ultimo ya que estan intercambiados
def leer_avance(archivo, pais):
    with open(archivo) as archivo:
        data = json.load(archivo)
        
        for i in data['info_vacunacion']:
            for j in data['info_vacunacion_fabricante']:
                if pais == i['Pais']:
                    print('Pais:', i['Pais'])
                    print('Anio:', i['Anio'])
                    print('Mes:', i['Mes'])
                    print('Numero absoluto de inmunizaciones:', i['Numero absoluto de inmunizaciones'])
                    print('Numero total de personas vacunadas:', i['Numero total de vacunaciones'])
                    print('Numero total de personas con el esquema completo:', i['Total de esquemas completos'])
                    print('Vacunas Utilizadas:', j['Tipos de vacunas'],'\n')
                    break

#Esta funcion permite leer un mes en especifico de un pais
def leer_avance_especifico(archivo, pais, mes):
    with open(archivo) as archivo:
        data = json.load(archivo)
        
        for i in data['info_vacunacion']:
            for j in data['info_vacunacion_fabricante']:    
                if pais == i['Pais'] and mes == i['Mes']:
                    print('Pais:', i['Pais'])
                    print('Anio:', i['Anio'])
                    print('Mes:', i['Mes'])
                    print('Numero absoluto de inmunizaciones:', i['Numero absoluto de inmunizaciones'])
                    print('Numero total de personas vacunadas:', i['Numero total de vacunaciones'])
                    print('Numero total de personas con el esquema completo:', i['Total de esquemas completos'])
                    print('Vacunas Utilizadas:', j['Tipos de vacunas'],'\n')    
                    break

def main():
    print('GENERANDO DATOS...')
    vacunas = "country_vaccinations.csv"
    vacunas_lab = "country_vaccinations_by_manufacturer.csv"
    conversor_archivo(vacunas, vacunas_lab)
    vacunas = "country_vacc.json"
    print('DATOS GENERADOS EXITOSAMENTE')
    print('Bienvenido a la base de datos vacunacion COVID-19')
    print('<1>Ver el avance mensual de vacunacion de un pais')
    print('<2>Ver el resumen de vacunacion de un pais segun el mes')
    print('<3>Comparar Dos Paises segun el mes')
    print('<0> Salir')
    decision =int(input()) 

    if decision == 1:
        pais = input("Ingrese el pais que desea ver:")
        pais = pais.lower()
        pais = pais.capitalize()
        leer_avance(vacunas, pais)

    if decision == 2:
        pais = input("Ingrese el pais que desea ver:")
        mes = input('Ingrese el mes (en numero):')
        pais = pais.lower()
        pais = pais.capitalize()
        leer_avance_especifico(vacunas, pais, mes)

    if decision == 3:
        pais = input("Ingrese el primer pais que desea ver:")
        mes = input('Ingrese el mes del pais (en numero):')
        pais2 = input("Ingrese el segundo pais que desea ver:")
        mes2 = input('Ingrese el mes del pais (en numero):')
        
        pais2 = pais2.lower()
        pais2 = pais2.capitalize()
        pais = pais.lower()
        pais = pais.capitalize()

        leer_avance_especifico(vacunas, pais, mes)
        leer_avance_especifico(vacunas, pais2, mes2)  

if __name__ == "__main__":
    main()



